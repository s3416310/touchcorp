/**
 * 
 */


import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import payment.Card;

/**
 * @author yohann
 *
 */
public class CardTest {
	Card mockCard = null;
	private final String mockCardHash = "AAAA";
	private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mockCard = new Card(mockCardHash);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test(expected=Error.class)
	public void testCardWithoutHashShouldFail() throws Error {
		@SuppressWarnings("unused")
		Card emptyCard = new Card("");
	}
	@Test(expected=Exception.class)
	public void testCardWithNullHashShouldFail() throws Exception {
		@SuppressWarnings("unused")
		Card nullCard = new Card(null);
	}
	@Test
	public void testNewCardShouldHaveNoTransactions() {
		int size = mockCard.getTransactions().size();
		assertTrue("Error, new card should not have any transactions", (size == 0));
	}
	@Test
	public void testCardHashShouldBeConstant() {
		assertTrue("Error, card hash should never change for a card", mockCardHash.equals(mockCard.getCardHash()));
	}
	@Test
	public void testSingleDayTransaction() throws ParseException {
		final int singleDayTransactionCount = 23;
		Card singleDayCard = new Card(mockCardHash);
		Date singleDay = formatter.parse("01/01/2016");
		Date anotherDay = formatter.parse("02/02/2016");
		
		for(int i = 0; i < singleDayTransactionCount; i++) {
			singleDayCard.addTransaction(singleDay, new BigDecimal("5.88"));
		}
		for(int i = 0; i < singleDayTransactionCount; i++) {
			singleDayCard.addTransaction(anotherDay, new BigDecimal("5.88"));
		}
		assertTrue("Error, transactions for a single day are not correct", (singleDayCard.getSingleDayTransactions(singleDay).size() == singleDayTransactionCount));
		assertTrue("Error, transactions for a single day are not correct", (singleDayCard.getSingleDayTransactions(anotherDay).size() == singleDayTransactionCount));
	}
	@Test
	public void testGetFraudulentTransaction() throws Exception {
		Card fraudCard = new Card(mockCardHash);
		Date singleDay = formatter.parse("01/01/2016");

		fraudCard.addTransaction(singleDay, new BigDecimal("5.00"));
		fraudCard.addTransaction(singleDay, new BigDecimal("12.00"));
		
		assertTrue(fraudCard.hasFradulentTransactions(new BigDecimal("0.00"), singleDay));
		assertTrue(fraudCard.hasFradulentTransactions(new BigDecimal("20.00"), singleDay) == false);
		assertTrue(fraudCard.hasFradulentTransactions(new BigDecimal("17.00"), singleDay) == false);
		assertTrue(fraudCard.hasFradulentTransactions(new BigDecimal("17.01"), singleDay) == false);
		assertTrue(fraudCard.hasFradulentTransactions(new BigDecimal(16.9), singleDay));
		assertTrue(fraudCard.hasFradulentTransactions(new BigDecimal("16.999"), singleDay));

	}
	@Test
	public void testAddTransactionToCard() {
		int currentTransactionSize = mockCard.getTransactions().size();
		int newTransactionSize;
		
		mockCard.addTransaction(new Date(), new BigDecimal("0.00"));
		newTransactionSize = mockCard.getTransactions().size();
		
		assertTrue("Error, card must only add a single transaction at a time", (newTransactionSize - currentTransactionSize) == 1);
		
	}
}
