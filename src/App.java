import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import payment.Card;

class TransactionProcessor {
	//10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 10.00

	// regex for transaction and date parser
	private static Pattern transactionPattern = Pattern.compile("([A-Za-z0-9]+), ([0-9\\-\\:T]+), ([0-9\\.]+)");
	private static SimpleDateFormat transactionDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	/**
	 * Processes transactions to determine fraudulent ones
	 * @param transactions An array list of transactions
	 * @param givenDay The particular date to search for fraudulent transactions
	 * @param priceThreshold The price threshold
	 * @return An array list <String> of all cards with fraudulent activity
	 */
	public static ArrayList<String> processTransactions(ArrayList<String> transactions, Date givenDay, String priceThreshold) {

		BigDecimal thresholdPrice = null;

		/* performance ;) */
		Hashtable<String, Card> table = new Hashtable<String, Card>();
		ArrayList<String> fradulentCardNumbers = new ArrayList<String>();

		try {
			/* Get threshold price */
			thresholdPrice = new BigDecimal(priceThreshold);

			/* Parse transactions from string to objects */
			parseTransactions(transactions, table);

			/* Check fraudulent transactions */
			Enumeration<String> tableKeys = table.keys();

			/* Iterate through cards */
			while(tableKeys.hasMoreElements()) {
				/* Get card from hash key */
				Card c = table.get(tableKeys.nextElement());

				/* Check if card has fraudulent transactions */
				if(c.hasFradulentTransactions(thresholdPrice, givenDay)) {
					/* Add to list of dodgy cards */
					fradulentCardNumbers.add(c.getCardHash());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return fradulentCardNumbers;
	}

	/**
	 * Parse a list of transaction strings and add to a specific card in the hash table (assuming card hash is unique)
	 * @param transactions The list of string transactions
	 * @param table Hash table to store it to
	 * @throws ParseException
	 */
	private static void parseTransactions(ArrayList<String> transactions,
			Hashtable<String, Card> table) throws ParseException {
		for(String transaction: transactions) {
			Matcher transactionMatch = TransactionProcessor.transactionPattern.matcher(transaction);

			if(transactionMatch.matches()) {					
				String currentHash = transactionMatch.group(1);

				/* If card does not already exist */
				if(table.get(currentHash) == null) {

					/* add card */
					table.put(currentHash, new Card(currentHash));
				}

				/* parse transaction date */
				Date tDate = transactionDateParser.parse(transactionMatch.group(2));

				/* get transaction price */
				BigDecimal tPrice = new BigDecimal(transactionMatch.group(3));

				/* get card */
				Card c = table.get(currentHash);

				/* Add transaction to card */
				c.addTransaction(tDate, tPrice);

				/* Put back card in table */
				table.put(currentHash, c);
			}
		}
	}
}
public class App {
	private static SecureRandom random = new SecureRandom();

	/**
	 * Generate a random 32 character card hash
	 * @return
	 */
	private static String generateRandomCardHash() {
		return new BigInteger(130, random).toString(32);
	}

	/**
	 * Generate a random price for a transaction for a card
	 * @return String price in 'dollar.cents' string format
	 */
	private static String generateRandomPrice() {
		DecimalFormat df = new DecimalFormat("#.##");
		float f = random.nextFloat()* (10 - 2) + 2;
		return new BigDecimal(df.format(f).toString()).toString();
	}

	/**
	 * Generates cards with random transactions
	 * @param transactions A list of string transactions will be added to this array list
	 */
	private static void generateRandomData(ArrayList<String> transactions) {
		/* Generate random card hashes  */
		for(int i =0; i < 100; i++) {

			/* Generate random transactions */
			String hash = generateRandomCardHash();


			String price = generateRandomPrice();

			/* Add transactions for a particular day with random amounts */
			for(int j = 0; j < 500; j++) {
				transactions.add(hash + ", 2014-04-29T13:15:54, " + price.toString());
			}

		}
	}

	/**
	 * main method, modify as you please
	 * @param args
	 */
	public static void main(final String [] args) {
		long startTime = System.currentTimeMillis();
		ArrayList<String> transactions = new ArrayList<String>();

		/* Generate random transactions */
		generateRandomData(transactions);

		/* Mock a certain date */
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

		/* 2500$ seems like a reasonable limit per day, adjust as you please :) */
		String threshold = "2500.00";

		try {
			ArrayList<String> cardHashes = TransactionProcessor.processTransactions(transactions, formatter.parse("2014/04/29"), threshold);

			/* Write to stdout */
			for(String hash: cardHashes) {
				System.out.println(hash);
			}
			long endTime = System.currentTimeMillis();
			System.out.println(cardHashes.size() + " fraudulent cards found in " + Long.toString(endTime - startTime) + " ms.");

		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}

