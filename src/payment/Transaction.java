package payment;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Class to represent a single date price pair transaction
 * @author yohann
 *
 */
public class Transaction {
	private Date timestamp;
	private BigDecimal price;
	
	/**
	 * Build a transaction from a timestamp and a price
	 * @param timestamp
	 * @param price
	 */
	public Transaction(Date timestamp, BigDecimal price) {
		this.timestamp = timestamp;
		this.price = price;
	}
	/**
	 * Get transaction date
	 * @return
	 */
	public Date getTimeStamp() {
		return timestamp;
	}
	/**
	 * Get transaction price
	 * @return
	 */
	public BigDecimal getPrice() {
		return price;
	}
}


