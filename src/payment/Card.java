package payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Class to represent a credit/debit card
 * @author yohann
 * 
 */
public class Card {
	private final String cardHash;
	private ArrayList<Transaction> transactions;
	
	/**
	 * Build a card from a card hash and initialize with no transactions
	 * @param cardHash
	 */
	public Card(String cardHash) {
		if(cardHash.length() < 1) {
			throw new Error("Card hash must have atleast one character");
		}
		this.cardHash = cardHash;
		this.transactions = new ArrayList<Transaction>();
	}
	/**
	 * Get the card hash identification string
	 * @return
	 */
	public String getCardHash() {
		return this.cardHash;
	}
	/**
	 * Get all transactions as an array list for a card
	 * @return
	 */
	public ArrayList<Transaction> getTransactions() {
		return transactions;
	}
	/**
	 * Add a transaction occurring on a date associated with a price
	 * @param d The date when the transaction happened
	 * @param a The price of the transaction
	 */
	public void addTransaction(Date d, BigDecimal a) {
		transactions.add(new Transaction(d, a));
	}
	
	/**
	 * Get all transactions for a single day
	 * @param givenDay The day to check for
	 * @return A list of transactions for the chosen day
	 */
	public ArrayList<Transaction> getSingleDayTransactions(Date givenDay) {
		
		/* Init list */
		ArrayList<Transaction> singleDayTransactions = new ArrayList<Transaction>();
		
		/* calendars for transaction dates and our chosen date */
		Calendar transactionCalendar = Calendar.getInstance();
		Calendar givenDayCalendar = Calendar.getInstance();

		/* Set chosen time */
		givenDayCalendar.setTime(givenDay);
		
		for(Transaction t: this.transactions) {
			
			/* Set transaction time */
			transactionCalendar.setTime(t.getTimeStamp());
			
			/* If day of year and year are same implies same day */
			if(transactionCalendar.get(Calendar.YEAR) == givenDayCalendar.get(Calendar.YEAR) &&
				transactionCalendar.get(Calendar.DAY_OF_YEAR) == givenDayCalendar.get(Calendar.DAY_OF_YEAR)) {
				
				/* Add to list */
				singleDayTransactions.add(t);
			}		
		}
		
		return singleDayTransactions;

	}
	
	/**
	 * Checks if a card is associated with fradulent transactions
	 * @param threshold
	 * @param givenDay
	 * @return
	 */
	public boolean hasFradulentTransactions(BigDecimal threshold, Date givenDay) {
		BigDecimal sum = new BigDecimal("0.00");
		
		/* Get all transactions for a particular day */
		ArrayList<Transaction> singleDayTransactions = getSingleDayTransactions(givenDay);
		
		/* Get sum of all the selected transactions */
		for(Transaction t: singleDayTransactions) {
			sum = (BigDecimal) sum.add(t.getPrice());
		}
		
		/* If sum is bigger than threshold implies dodgy stuff :-o */
		if(sum.compareTo(threshold) == 1)
			return true;
		
		return false;
	}
}
